<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\IQuoteRepo;

class QuoteController extends Controller
{
    private IQuoteRepo $quoteRepo;
    public function __construct(IQuoteRepo $quoteRepo)
    {
        $this->quoteRepo= $quoteRepo;
    }
    public function index()
    {
        $quote = $this->quoteRepo->all();
        dd($quote);
    }
}
