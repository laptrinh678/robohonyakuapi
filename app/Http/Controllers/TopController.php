<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Mail\Mailable;
class TopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$audiofile = "";
		if(Session::exists('audiofile')){
			$audiofile = Session::pull('audiofile');
		}
		//var_dump($audiofile);
        return view('frontend.top', compact('audiofile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendmail()
    {   
       // 1. xác thực 2 lớp
        //https://myaccount.google.com/security?rapt=AEjHL4Ojq23uGGdK8a4o-UbytOWvkhKO3afhHTQQ_U9FVkGW4nuRbo3Qo2owqc8ImTNLCUIkTNxX2dVVt-xUrWQdChiZ-bY-KQ
        //2. tạo app mail
        //https://myaccount.google.com/apppasswords?gar=1&rapt=AEjHL4OUImDdM_-cjOICxGSg22Dmf632_BGiu3RpNjbkv_MgPGKGKmHwdbfhjFfWt6XZ1T8ObewNN1TWQupa43JriaZmjj7LWw
        //dd('aa');
        $data['email'] ='laptrinh678@gmail.com';
        $email = $data['email'];
        $data['name'] ='lapvt';
        //dd($data);
        \Mail::send('frontend.sendemail',$data, function($msg) use ($email)
        {
            $msg->from('lapvu.adnetplus@gmail.com','Nextscience');
            $msg->to($email);
           // $msg->subject($subject);
            
        });
        dd('ok');
        // Mail::to('laptrinh678@gmail.com')->send(new MailNotify($this->data));
    }
}
