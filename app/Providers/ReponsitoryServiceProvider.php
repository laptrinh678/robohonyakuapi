<?php

namespace App\Providers;
use App\Repository\QuoteRepo;
use App\Repository\IQuoteRepo;
use Illuminate\Support\ServiceProvider;

class ReponsitoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //dd('aaa');
        $this->app->bind(IQuoteRepo::class,QuoteRepo::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
