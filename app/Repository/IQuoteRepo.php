<?php 
namespace App\Repository;

interface IQuoteRepo
{
    public function all();
    public function getDetail();
}