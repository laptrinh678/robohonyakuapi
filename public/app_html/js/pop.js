$(function() {

    //js-modal-openをクリックで表示
    $('.js-modal-open').on('click', function() {
        $('.js-modal').fadeIn("fast");
        return false;
    });
    //js-modal-closeをクリックで非表示
    $('.js-modal-close').on('click', function() {
        $('.js-modal').fadeOut("fast");
        return false;
    });
    //js-modal-open2をクリックで表示
    $('.js-modal-open2').on('click', function() {
        $('.js-modal2').fadeIn("fast");
        return false;
    });
    //js-modal-closeをクリックで非表示
    $('.js-modal-close2').on('click', function() {
        $('.js-modal2').fadeOut("fast");
        return false;
    });

    $('#redirect_record').on('click', function(event) {
        var language = $("input[name~='language']:checked").map(function() { return $(this).val(); }).get();
        sessionStorage.setItem("language", language);
        var language2 = $("input[name~='language2']:checked").map(function() { return $(this).val(); }).get();
        sessionStorage.setItem("language2", language2);
        window.location.href = '/record.html';
    });
    var sesion_language = sessionStorage.getItem("language");
    var sesion_language2 = sessionStorage.getItem("language2");
    $('#language').val(sesion_language);
    $('#language2').val(sesion_language2);

});

function speak() {
    var speak = new SpeechSynthesisUtterance();
    speak.text = document.querySelector('.read_text').value;
    speak.rate = 1; // 読み上げ速度 0.1-10 初期値:1 (倍速なら2, 半分の倍速なら0.5, )
    speak.pitch = 1;　 // 声の高さ 0-2 初期値:1(0で女性の声) 
    speak.lang = 'ja-JP'; //(日本語:ja-JP, アメリカ英語:en-US, イギリス英語:en-GB, 中国語:zh-CN, 韓国語:ko-KR)

    sleep(2000);
    speechSynthesis.speak(speak);

    console.log(speak.text);
}

function sleep(time) {
    var date_1 = new Date().getTime();
    var date_2 = new Date().getTime();
    while (date_2 < date_1 + time) {
        date_2 = new Date().getTime();
    }
    return;
};