
//TOPの画像用
$('.slider').slick({
    //autoplay:true,
    autoplay:false,
    autoplaySpeed:5000,
    infinite: true,//スライドをループさせるかどうか。初期値はtrue。
    slidesToShow: 1,//スライドを画面に1枚見せる
    slidesToScroll: 1,//1回のスクロールで1枚の写真を移動して見せる
    dots: true,//下部ドットナビゲーションの表示
    arrows:false,
});