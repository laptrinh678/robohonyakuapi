<!doctype html>
<html>
<!-- InstanceBegin template="/Templates/Untitled-3.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta name="format-detection" content="telephone=no">
    <!-- InstanceBeginEditable name="doctitle" -->
    <title> @yield('title')</title>
    @yield('style')
</head>
<body>
    @include('frontend.header')
    @yield('content')
    @yield('script')
</body>

</html>