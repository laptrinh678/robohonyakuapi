@extends('frontend.layout')
@section('title')
無題ドキュメント
@endsection('title')
@section('style')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('app_html/recorder/style.css')}}">
<link rel="stylesheet" href="{{asset('app_html/css/destyle.css')}}">
<link rel="stylesheet" href="{{asset('app_html/css/template.css')}}">
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" href="{{asset('app_html/css/top.css')}}">
<!-- InstanceEndEditable -->
<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
@endsection('style')
@section('content')
<form id="frmIssue" action="" enctype="multipart/form-data" method="post">
    <div id="controls">
        <!-- <button id="recordButton">Record</button>  -->
        <!-- <button id="pauseButton" disabled>Pause</button> -->
        <!-- <button id="stopButton" disabled>Stop</button> -->
    </div>
    <div id="formats">Format: start recording to see sample rate</div>
    <!-- <p><strong>Recordings:</strong></p>  -->
    <ol id="recordingsList"> </ol>
    <div class="record_button">
        <a href="javascript:void(0);" id="stopButton"><img src="{{url('app_html/images')}}/record.png"></a>
    </div>

    <input type="hidden" id="language" name="language" value="">
    <input type="hidden" id="language2" name="language2" value="">
    <textarea class="hidden" id="blob" name="blob"></textarea>
    {{csrf_field()}}
</form>
<!-- inserting these scripts at the end to be able to use all the elements in the DOM -->
</div>
@endsection('content')
@section('script')
<script src="{{asset('app_html/recorder/app.js')}}"></script>
<script src="{{asset('app_html/recorder/recorder.js')}}"></script>
<script>
$(document).ready(function() {
    var sesion_language = sessionStorage.getItem("language");
    var sesion_language2 = sessionStorage.getItem("language2");
    $('#language').val(sesion_language);
    $('#language2').val(sesion_language2);
    var timebig = sessionStorage.getItem("timebig");
    console.log(timebig);
    if (timebig == 1) {
        alert('記録時間制限を超えました');
    }

})
</script>
@endsection('script')