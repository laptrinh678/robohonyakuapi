 <!-- InstanceBeginEditable name="EditRegion" -->
 @extends('frontend.layout')
 @section('title')
 無題ドキュメント
 @endsection('title')
 @section('style')
 <link rel="stylesheet" href="{{asset('app_html/css/destyle.css')}}">
 <link rel="stylesheet" href="{{asset('app_html/css/template.css')}}">
 <!-- InstanceEndEditable -->
 <!-- InstanceBeginEditable name="head" -->
 <link rel="stylesheet" href="{{asset('app_html/css/top.css')}}">
 <link rel="stylesheet" href="{{asset('app_html/css/pop.css')}}">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
 <script src="{{asset('app_html/js/pop.js')}}"></script>
 <!-- InstanceEndEditable -->
 @endsection('style')

 @section('content')
 <div class="main_inner">
     <div class="before_text"><a id="redirect_record" href="javascript:void(0);"><img
                 src="{{url('app_html/images')}}/record.png" class="record_img"></a>
         <div class="box_height">
             <div class="sound"> <a onclick="beeb.play();" href="javascript:void(0)"><img
                         src="{{url('app_html/images')}}/sound.png"></a>
                 <p>日本語</p>
             </div>
             <textarea placeholder="テキストを入力" id="texttalk"></textarea>
         </div>
     </div>
     <div class="language">
         <p class="js-modal-open">日本語</p>
         <img src="{{url('app_html/images')}}/yazirusi.png">
         <p class="js-modal-open2">英語</p>
     </div>
     <div class="after_text">
         <div class="box_height">
             <div class="sound"><img src="{{url('app_html/images')}}/sound.png" onclick="speak()">
                 <p>英語</p>
             </div>
             <textarea class="read_text"></textarea>
         </div>
     </div>
 </div>
 <div id="audio"></div>
 <!--↓ポップアップの部分-->
 <div class="modal js-modal">
     <div class="modal_bg"></div>
     <div class="modal_content">
         <div class="modal_inner">
             <div class="js-modal-close"></div>
             <div>
                 <div class="inline_org">
                     <input type="radio" name="language" value="1" id="insurance_check" checked>
                     <label class="occupation_label" for="insurance_check">日本語</label>
                 </div>
                 <div class="inline_org">
                     <input type="radio" name="language" value="2" id="insurance_check2">
                     <label class="occupation_label" for="insurance_check2">英語</label>
                 </div>
                 <div class="inline_org">
                     <input type="radio" name="language" value="3" id="insurance_check3">
                     <label class="occupation_label" for="insurance_check3">中国語</label>
                 </div>
                 <div class="inline_org">
                     <input type="radio" name="language" value="4" id="insurance_check4">
                     <label class="occupation_label" for="insurance_check4">台湾語</label>
                 </div>
                 <div class="inline_org">
                     <input type="radio" name="language" value="5" id="insurance_check5">
                     <label class="occupation_label" for="insurance_check5">スペイン語</label>
                 </div>
                 <div class="inline_org">
                     <input type="radio" name="language" value="6" id="insurance_check6">
                     <label class="occupation_label" for="insurance_check6">ベトナム語</label>
                 </div>
             </div>
         </div>
     </div>
 </div>
 <div class="modal js-modal2">
     <div class="modal_bg"></div>
     <div class="modal_content">
         <div class="modal_inner">
             <div class="js-modal-close2"></div>
             <div>
                 <div class="inline_org">
                     <input type="radio" name="language2" value="7" id="insurance_check7">
                     <label class="occupation_label" for="insurance_check7">日本語</label>
                 </div>
                 <div class="inline_org">
                     <input type="radio" name="language2" value="8" id="insurance_check8" checked>
                     <label class="occupation_label" for="insurance_check8">英語</label>
                 </div>
                 <div class="inline_org">
                     <input type="radio" name="language2" value="9" id="insurance_check9">
                     <label class="occupation_label" for="insurance_check9">中国語</label>
                 </div>
                 <div class="inline_org">
                     <input type="radio" name="language2" value="10" id="insurance_check10">
                     <label class="occupation_label" for="insurance_check10">台湾語</label>
                 </div>
                 <div class="inline_org">
                     <input type="radio" name="language2" value="11" id="insurance_check11">
                     <label class="occupation_label" for="insurance_check11">スペイン語</label>
                 </div>
                 <div class="inline_org">
                     <input type="radio" name="language2" value="12" id="insurance_check12">
                     <label class="occupation_label" for="insurance_check12">ベトナム語</label>
                 </div>
             </div>
         </div>
     </div>
     <input type="hidden" value="{{url('')}}" id="url">
 </div>
 @endsection('content')
 @section('script')
 <!-- InstanceEndEditable -->
 <script>
$(document).ready(function() {
    if (typeof(Storage) !== "undefined") {
        var audiofile = localStorage.getItem("audiofile");
        if (audiofile) {

            window.beeb = new Audio('data:audio/wav;base64,' + audiofile);

            var audio = document.getElementById('audio'),
                au = document.createElement('audio');
            //add controls to the <audio> element
            au.controls = false;
            au.src = 'data:audio/wav;base64,' + audiofile;
            //add the new audio element to li
            audio.appendChild(au);
            //localStorage.removeItem("audiofile");
            sessionStorage.clear();
        }
    }
});

function play() {
    var audio = document.getElementById("audio");
    audio.play();
}
 </script>
 @endsection('script')