<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () 
// {
//     return view('');
// });

Route::get('/', 'TopController@index')->name('top');
Route::get('record.html', 'RecordController@index')->name('record');
Route::post('record.html', 'RecordController@store')->name('create');
Route::get('sendmail', 'TopController@sendmail')->name('sendmail');
Route::get('quote', 'QuoteController@index');